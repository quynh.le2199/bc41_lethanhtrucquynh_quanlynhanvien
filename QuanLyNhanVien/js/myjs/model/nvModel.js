function nhanVien(_taiKhoan, _hoTen,  _email, _matKhau, _ngayLam, _luongCoBan, _chucVu, _gioLam) {
    this.taiKhoan =_taiKhoan;
    this.hoTen = _hoTen;
    this.email = _email;
    this.matKhau = _matKhau;
    this.ngayLam = _ngayLam;
    this.luongCoBan = _luongCoBan;
    this.chucVu = _chucVu;
    this.gioLam = _gioLam;
    this.tongLuong = function() {
        var tongLuong;
        if (this.chucVu == "Sếp") {
            tongLuong = this.luongCoBan * 3;
        }
        else if (this.chucVu == "Trưởng phòng") {
            tongLuong = this.luongCoBan * 2;
        }
        else if (this.chucVu == "Nhân viên") {
            tongLuong = this.luongCoBan * 1;
        }
        return tongLuong;
    };
    this.xepLoai = function() {
        var xepLoai;
        if (this.gioLam < 160) {
            xepLoai = "Trung Bình";
        }
        else if (this.gioLam < 176) {
            xepLoai = "Khá";
        }
        else if (this.gioLam < 192) {
            xepLoai = "Giỏi";
        }
        else {
            xepLoai = "Xuất sắc";
        }
        return xepLoai;
    };
}