var DSNV = [];

var dataJson = localStorage.getItem("DSNV_LOCAL");
if (dataJson != null) {
    var dataArr = JSON.parse(dataJson);
    DSNV = dataArr.map(function(item) {
        var nv = new nhanVien(
            item.taiKhoan,
            item.hoTen,
            item.email,
            item.matKhau,
            item.ngayLam,
            item.luongCoBan,
            item.chucVu,
            item.gioLam,)
        return nv;
    })
    renderDSNV(DSNV);
}

function themNV() {
    document.getElementById("tknv").disabled = false;
    var nv = layThongTinTuForm();
    var isValidTaiKhoan = true;
    isValidTaiKhoan = kiemTraTrong(nv.taiKhoan, "tbTKNV") && kiemTraTrung(nv.taiKhoan, DSNV) && kiemTraDoDai(nv.taiKhoan,"tbTKNV", 4, 6);
    var isValidTen = true;
    isValidTen =  kiemTraTrong(nv.hoTen, "tbTen") && kiemTraChu(nv.hoTen);
    var isValidEmail = true;
    isValidEmail = kiemTraTrong(nv.email, "tbEmail") && kiemTraEmail(nv.email);
    var isValidMatKhau = true;
    isValidMatKhau = kiemTraTrong(nv.matKhau, "tbMatKhau") && kiemTraMatKhau(nv.matKhau);
    var isValidNgay = true;
    isValidNgay = kiemTraTrong(nv.ngayLam, "tbNgay") && kiemTraNgay(nv.ngayLam);
    var isValidLuong = true;
    isValidLuong = kiemTraTrong(nv.luongCoBan, "tbLuongCB") && kiemTraSo(nv.luongCoBan, "tbLuongCB") && kiemTraGioiHanLuong(nv.luongCoBan, 1000000, 20000000);
    var isValidChucVu = true;
    isValidChucVu = kiemTraChucVu(nv.chucVu);
    var isValidGioLam = true;
    isValidGioLam = kiemTraTrong(nv.gioLam, "tbGiolam") && kiemTraSo(nv.gioLam, "tbGiolam") && kiemTraGioiHanGioLam(nv.gioLam, 80, 200);
    var isValid = isValidTaiKhoan & isValidTen & isValidEmail & isValidNgay & isValidLuong & isValidChucVu & isValidGioLam;
    if (isValid) {
        DSNV.push(nv);
        var dsnvJson = JSON.stringify(DSNV);
        localStorage.setItem("DSNV_LOCAL", dsnvJson);
        renderDSNV(DSNV);
    }
}

function xoaNV(taiKhoanNV) {
    var viTri = timKiemViTri(taiKhoanNV, DSNV);
    if (viTri != -1) {
        DSNV.splice(viTri, 1);
        renderDSNV(DSNV);
    }
}

function suaNV(taiKhoanNV) {
    var viTri = timKiemViTri(taiKhoanNV, DSNV);
    if (viTri != -1) {
        var nv = DSNV[viTri];
        document.getElementById("tknv").value = nv.taiKhoan;
        document.getElementById("name").value = nv.hoTen;
        document.getElementById("email").value = nv.email;
        document.getElementById("password").value = nv.matKhau;
        // document.getElementById("ngaylam").value = nv.ngayLam;
        document.getElementById("luongCB").value = nv.luongCoBan;
        document.getElementById("chucvu").value = nv.chucVu;
        document.getElementById("gioLam").value = nv.gioLam;
        document.getElementById("tknv").disabled = true;
    }
}

function capNhatNV(taiKhoanNV) {
    var nv = layThongTinTuForm();
    var isValidTen = true;
    isValidTen =  kiemTraTrong(nv.hoTen, "tbTen") && kiemTraChu(nv.hoTen);
    var isValidEmail = true;
    isValidEmail = kiemTraTrong(nv.email, "tbEmail") && kiemTraEmail(nv.email);
    var isValidMatKhau = true;
    isValidMatKhau = kiemTraTrong(nv.matKhau, "tbMatKhau") && kiemTraMatKhau(nv.matKhau);
    var isValidNgay = true;
    isValidNgay = kiemTraTrong(nv.ngayLam, "tbNgay") && kiemTraNgay(nv.ngayLam);
    var isValidLuong = true;
    isValidLuong = kiemTraTrong(nv.luongCoBan, "tbLuongCB") && kiemTraSo(nv.luongCoBan, "tbLuongCB") && kiemTraGioiHanLuong(nv.luongCoBan, 1000000, 20000000);
    var isValidChucVu = true;
    isValidChucVu = kiemTraChucVu(nv.chucVu);
    var isValidGioLam = true;
    isValidGioLam = kiemTraTrong(nv.gioLam, "tbGiolam") && kiemTraSo(nv.gioLam, "tbGiolam") && kiemTraGioiHanGioLam(nv.gioLam, 80, 200);
    var isValid = isValidTen & isValidEmail & isValidNgay & isValidLuong & isValidChucVu & isValidGioLam;
    if (isValid) {
        var viTri = timKiemViTri(taiKhoanNV, DSNV);
        if (viTri != -1) {
            DSNV.splice(viTri, 1);
        }
        DSNV.push(nv);
        var dsnvJson = JSON.stringify(DSNV);
        localStorage.setItem("DSNV_LOCAL", dsnvJson);
        renderDSNV(DSNV);
    }
}

function tiemKiemNV() {
    var loaiTimKiem = document.getElementById("searchName").value;
    loaiTimKiem.toLowerCase();
    var xepLoai = document.getElementById("")

    var viTri = timKiemViTri(taiKhoanNV, DSNV);
        if (viTri != -1) {
            var xepLoaiNV = DSNV.filter(function(item) {
                item = loaiTimKiem;
                
            })
        }
    
}

function test() {
    document.getElementById("test").innerHTML = "12345";
}