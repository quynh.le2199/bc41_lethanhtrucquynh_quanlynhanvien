function kiemTraTrung(idNV, nvArr) {
    var viTri = nvArr.findIndex(function(item) {
        return item.taiKhoan == idNV;
    });
    if (viTri != -1) {
        document.getElementById("tbTKNV").innerText = "Tài khoản đã tồn tại";
        document.getElementById("tbTKNV").style.display = "inline-block";
        return false;
    }
    else {
        document.getElementById("tbTKNV").style.display = "none";
        return true;
    }
}

function kiemTraTrong(value, hienThiErr) {
    if (value == "") {
        document.getElementById(hienThiErr).innerText = "Mục không được để trống";
        document.getElementById(hienThiErr).style.display = "inline-block";
        return false;
    }
    else {
        document.getElementById(hienThiErr).style.display = "none";
        return true;
    }
}

function kiemTraDoDai(value,hienThiErr, min, max) {
    var length = value.length;
    if (length < min || length > max) {
        document.getElementById(hienThiErr).innerText = `Độ dài phải từ ${min} đến ${max} kí tự`;
        document.getElementById(hienThiErr).style.display = "inline-block";
        return false;
    }
    else {
        document.getElementById(hienThiErr).innerText = "";
        return true;
    }
}

function kiemTraChu(value) {
    const re = /^[a-zA-Z\s]*$/;
    var isLetter = re.test(value);
    if (isLetter) {
        document.getElementById("tbTen").innerText = "";
        return true;
    }
    else {
        document.getElementById("tbTen").innerText = `Tên nhân viên phải là chữ (không dấu)`;
        document.getElementById("tbTen").style.display = "inline-block";
        return false;
    }
}

function kiemTraEmail(value) {
    const re =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail = re.test(value);
    if (isEmail) {
        document.getElementById("tbEmail").innerText = "";
        return true;
    }
    else {
        document.getElementById("tbEmail").innerText = `Email không đúng định dạng`;
        document.getElementById("tbEmail").style.display = "inline-block";
        return false;
    }
}
function kiemTraMatKhau(value) {
    const re = /^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{6,10}$/;
    var isMatKhau = re.test(value);
    if (isMatKhau) {
        document.getElementById("tbMatKhau").style.display = "";
        return true;
    }
    else {
        document.getElementById("tbMatKhau").innerText = `Mật khẩu phải từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)`;
        document.getElementById("tbMatKhau").style.display = "inline-block";
        return false;
    }
}

function kiemTraNgay(value) {
    const re = /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/;
    var isDate = re.test(value);
    if (isDate) {
        document.getElementById("tbNgay").innerText = "";
        return true;
    }
    else {
        document.getElementById("tbNgay").innerText = `Ngày không đúng định dạng`;
        document.getElementById("tbNgay").style.display = "inline-block";
        return false;
    }
}

function kiemTraSo(value,hienThiErr) {
    const re = /^\d+$/;
    var isNumber = re.test(value);
    if (isNumber) {
        document.getElementById(hienThiErr).innerText = "";
        return true;
    }
    else {
        document.getElementById(hienThiErr).innerText = `Giá trị nhập phải có định dạng số`;
        document.getElementById(hienThiErr).style.display = "inline-block";
        return false;
    }
}

function kiemTraGioiHanLuong(value, min, max) {
    if (value >= min && value <= max) {
        return true;
    }
    else {
        document.getElementById("tbLuongCB").innerText = `Lương cơ bản phải từ ${min} - ${max}đ`;
        document.getElementById("tbLuongCB").style.display = "inline-block";
        return false
    }
}

function kiemTraChucVu(value) {
    if (value == "Sếp" || value == "Trưởng phòng" || value == "Nhân viên") {
        document.getElementById("tbChucVu").style.display = "";
        return true;
    }
    else {
        document.getElementById("tbChucVu").innerText = `Vui lòng chọn chức vụ`;
        document.getElementById("tbChucVu").style.display = "inline-block";
        return false;
    }
}

function kiemTraGioiHanGioLam(value, min, max) {
    if (value >= min && value <= max) {
        return true;
    }
    else {
        document.getElementById("tbGiolam").innerText = `Giờ làm phải từ ${min} - ${max} giờ`;
        document.getElementById("tbGiolam").style.display = "inline-block";
        return false
    }
}

