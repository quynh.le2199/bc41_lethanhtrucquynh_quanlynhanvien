function layThongTinTuForm() {
    var _taiKhoan = document.getElementById("tknv").value;
    var _hoTen = document.getElementById("name").value;
    var _email = document.getElementById("email").value;
    var _matKhau = document.getElementById("password").value;
    var _ngayLam = document.getElementById("datepicker").value;
    var _luongCoBan = document.getElementById("luongCB").value;
    var _chucVu = document.getElementById("chucvu").value;
    var _gioLam = document.getElementById("gioLam").value;

    return new nhanVien(_taiKhoan, _hoTen,  _email, _matKhau, _ngayLam, _luongCoBan, _chucVu, _gioLam)
}

function renderDSNV(nvArr) {
    var contentHTML = "";
    for (var i = 0; i < nvArr.length; i++) {
        var nv = nvArr[i];
        var contentTr = `<tr>
                            <td>${nv.taiKhoan}</td>
                            <td>${nv.hoTen}</td>
                            <td>${nv.email}</td>
                            <td>${nv.ngayLam}</td>
                            <td>${nv.chucVu}</td>
                            <td>${nv.tongLuong()}</td>
                            <td>${nv.xepLoai()}</td>
                            <td>
                                <button onclick="suaNV('${nv.taiKhoan}')" data-toggle="modal"
                                data-target="#myModal" class="btn btn-warning my-1">Sửa</button>
                                <button onclick="xoaNV('${nv.taiKhoan}')" class="btn btn-danger my-1">Xóa</button>
                            </td>
                         </tr>`;
        contentHTML += contentTr;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function timKiemViTri(id, arr) {
    var viTri = -1;
    for (var i = 0; i< arr.length; i++) {
        if (arr[i].taiKhoan == id) {
            viTri = i;
        }
    }
    return viTri;
}